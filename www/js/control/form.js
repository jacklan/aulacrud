$( document ).ready( function() 
{ 

        key = localStorage.getItem('CODIGO' );


        dati.initialize(function(status){
            if(status==false){
                alert("Não foi possível se conectar ao BD.");
            }
        });
        
        // Editar contato
        if (key) {

            var sql = "select * from PESSOA where CODIGO="+key;
            
            dati.query(sql,function(record){
               
                var obj = record.rows.item(0);

                $('#nome').val( obj['NOME'] );
                $('#sobrenome').val( obj['SOBRENOME'] );
                $(document).find('select').val( obj['SEXO'] );
                $('#telefone').val( obj['TELEFONE'] );
                $('#datanascimento').val( obj['DATANASCIMENTO'] );
                $('#longetude').val( obj['LONGETUDE'] );
                $('#latitude').val( obj['LATITUDE'] );
                $('#imagem').attr('src',obj['IMAGEM']);
                 
                Materialize.updateTextFields();
                $('select').material_select();

            });

        }


        // Salvar/Editar contato
        $(document).on("click", "#save", function(evt)
        { 
           
            var record = { };
            record['nome']      = $('#nome').val();
            record['sobrenome'] = $('#sobrenome').val();
            record['sexo']      = $(document).find('select').val();
            record['telefone']  = $('#telefone').val();
            record['datanascimento']  = $('#datanascimento').val();
            record['longetude']  = $('#longetude').val();
            record['latitude']  = $('#latitude').val(); 
            record['imagem']  = $('#imagem').attr('src'); 
            
            if (key) {

                dati.update('PESSOA', record,"CODIGO",key,function(status){
                    alert('Usario editato com sucesso');
                    window.location.href = 'index.html';
                });

            }else{

                dati.insert('PESSOA', record, function(key){   
                    alert('Usario salvo com sucesso');
                    window.location.href = 'index.html';
                });

            }

        });


});

//metodo para pegar o GPS 
    $(document).on("click", "#gps", function (evt)
    {
        
        //---------------------------------------------------------
        //criando função e armazenando dentro da variavel onSuccess os valores dos campos Latitude e Longitude
        var onSuccess = function (position) {

            $("#latitude").val(position.coords.latitude);
            $("#longetude").val(position.coords.longitude);

        };
        
        //---------------------------------------------------------
        // onError Callback receives a PositionError object
        function onError(error) {
            alert('code: ' + error.code + '\n' +
                    'message: ' + error.message + '\n');
        }
        
        navigator.geolocation.getCurrentPosition(onSuccess, onError);
    });



    
function captureFoto() {
  
    var onCapturar = function(imageSrc){
        var item = '<img id="imagem" src="data:image/jpeg;base64,'+imageSrc+'" style="width:100%; !important"/>';
        
       
        //alert(item);
        console.log( item );
        
        $("#imageOut").append(item);//adiciona a nova imagem
        $( "#imagem" ).remove();// remove a imagem atual 
    };
    
	
	function onfalhaCamera(message) {
        alert('Failed because: ' + message);
    }
    
    var opcoes = { 
        quality: 75,
        destinationType: Camera.DestinationType.DATA_URL 
    };
    
    navigator.camera.getPicture(onCapturar, onfalhaCamera, opcoes);
    
}



//---------------------------------------------------------

document.addEventListener("deviceready", function(){
        

}, false);





