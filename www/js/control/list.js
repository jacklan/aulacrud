$( document ).ready( function() 
{ 

    localStorage.setItem('CODIGO', '' );

    dati.initialize(function(status){
        if(status==false){
            alert("Não foi possível se conectar ao BD.");
        }
    });
        

    // Listar contatos
    dati.selectAll('PESSOA',function(record){

        $("#list").empty();
        var item = '';

        $.each( record, function( key, val ) 
        {
            item += '<li id="'+ val.CODIGO +'" tel="'+ val.TELEFONE +'" class="collection-item" >';
            item += '    <div>' + val.NOME + ' ' + val.SOBRENOME;
            item += '      <a id="editar"  CODIGO="'+ val.CODIGO +'" href="#!" class="secondary-content">editar</a>';
            item += '      <a id="remover" CODIGO="'+ val.CODIGO +'" href="#!" class="secondary-content">remover</a>';
            item += '    </div>';
            item += '</li>';
        });

        $("#list").prepend(item);

    });   


    // Remover contato
    $(document).on("click", "#remover", function(evt)
    { 
        var key = $(this).attr("CODIGO");

        dati.delete('PESSOA',"CODIGO",key,function(status){
            Materialize.toast('Usario deletado com sucesso', 2000);
        });

        $('#'+key).slideUp('normal', function(){
                $('#'+key).remove();
        });

    });

    // Editar contato
    $(document).on("click", "#editar", function(evt)
    { 
        localStorage.setItem('CODIGO', $(this).attr("CODIGO") );
        window.location.href = 'cadastro.html';
    });


    // Ligar contato
    $(document).on("dblclick", ".collection-item", function(evt)
    { 

    window.open('tel:' + $(this).attr('tel') ,'_self', 'location=no');

    });

});

document.addEventListener("deviceready", function(){
        

}, false);







