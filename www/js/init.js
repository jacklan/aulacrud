(function($){
  $(function(){

    $('.button-collapse').sideNav();
    $('select').material_select();
    $('.modal-trigger').leanModal();

  $('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15,
    format: 'dd/mm/yyyy' // Creates a dropdown of 15 years to control year
  });

  }); // end of document ready
})(jQuery); // end of jQuery name space

